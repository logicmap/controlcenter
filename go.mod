module gitlab.com/lightmeter/controlcenter

go 1.14

require (
	github.com/dlclark/regexp2 v1.2.0 // indirect
	github.com/gorilla/sessions v1.2.0 // indirect
	github.com/hpcloud/tail v1.0.0
	github.com/mattn/go-sqlite3 v1.13.1-0.20200606034938-baaf8a978416
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
	github.com/trustelem/zxcvbn v1.0.1 // indirect
	gitlab.com/lightmeter/postfix-log-parser v0.0.0-20200722164326-f274d5c51ec9
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
